﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBehaviour : MonoBehaviour {
    public GameObject Player;
    //Modus1: moving Platform; Modus2: disapearing Platform
    public int modus;
    //Velocity of moving Platform; Increased in GameControl over time
    public static float xVel=2.6f;
    float blinklTimer = 0;
	// Use this for initialization
	void Start () {
        
        Player = GameObject.Find("Player");
        if (modus == 1) {
            
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(xVel, 0, 0);
        }
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Player.transform.position.y - gameObject.transform.position.y > 20) Destroy(gameObject);
        if (modus == 1) {
            
            
         //Change movingDirection at Border   
        if (gameObject.transform.position.x < Player.GetComponent<PlayerBehaviour>().frustumCorners[0].x+gameObject.transform.localScale.x/2 ) gameObject.GetComponent<Rigidbody>().velocity =new Vector3(xVel,0,0);
        if(gameObject.transform.position.x > -1 * Player.GetComponent<PlayerBehaviour>().frustumCorners[0].x - gameObject.transform.localScale.x/2) gameObject.GetComponent<Rigidbody>().velocity = new Vector3(-xVel, 0, 0);
        }
      

    }

    private void FixedUpdate()
    {
        //Blinking of single-use Platforms
        blinklTimer += Time.deltaTime;
        if (modus==2 && blinklTimer >= 0.3f ) {
            
            blinklTimer = 0;
          if(  gameObject.GetComponent<SpriteRenderer>().enabled ==true) gameObject.GetComponent<SpriteRenderer>().enabled=false;
           else if (gameObject.GetComponent<SpriteRenderer>().enabled == false) gameObject.GetComponent<SpriteRenderer>().enabled = true;

        }
    }
    private void OnBecameInvisible()
    {
        
       //Destroy Platforms out of view

        if (gameObject.transform.position.y < Camera.main.transform.position.y+Player.GetComponent<PlayerBehaviour>().frustumCorners[0].y)
        {
            

            Destroy(gameObject);
        }
    }
 

}
