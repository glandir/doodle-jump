﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Showing the current score
public class Score : MonoBehaviour {
    public Text Text;
	// Use this for initialization
	void Start () {
        
        if (GameControl.points != 0) Text.text = "Score: " + (GameControl.points+GameControl.bonus);
	}
	
	// Update is called once per frame
	
}

