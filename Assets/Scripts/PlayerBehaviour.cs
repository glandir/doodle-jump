﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour
{
 


//Player State: RocketItem->flying 
    public enum gameState
    {
        normal,
        flying
    }

    public static gameState state;
    public GameObject expl;
    private AudioSource jumpSound;
    private AudioSource growl;
    private AudioSource explosion;
    private AudioSource losing;
    public static string DebugText;
    public Vector3[] frustumCorners = new Vector3[4];
    public float jumpSpeed;
    public float sideSpeed;

    //Acceleration Variables
    private static float AccelerometerUpdateInterval = 1.0f / 60.0f;
    private static float LowPassKernelWidthInSeconds = 0.5f;
    private float LowPassFilterFactor = AccelerometerUpdateInterval / LowPassKernelWidthInSeconds;
    private Vector3 direction = Vector3.zero;
    private Vector3 lowPassValue = Vector3.zero;

    // Use this for initialization
    void Start()
    {

        lowPassValue = Input.acceleration;
        DebugText = " " + lowPassValue;

        jumpSound = gameObject.GetComponent<AudioSource>();
        state = gameState.normal;
        growl = this.gameObject.transform.GetChild(1).gameObject.GetComponent<AudioSource>();
        explosion = gameObject.transform.GetChild(0).gameObject.GetComponent<AudioSource>();
        losing = gameObject.transform.GetChild(2).gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //flying state
        if (state == gameState.flying) gameObject.GetComponent<Rigidbody>().velocity = new Vector3(gameObject.GetComponent<Rigidbody>().velocity.x, 10, 0);
        //CameraViewFrustum
        Camera.main.CalculateFrustumCorners(new Rect(0, 0, 1, 1), Mathf.Abs(Camera.main.transform.position.z), Camera.MonoOrStereoscopicEye.Mono, frustumCorners);
        for (int i = 0; i < frustumCorners.Length; i++)
        {
            Debug.Log("" + frustumCorners[i]);
        }
        if (gameObject.transform.position.y < (Camera.main.transform.position.y + frustumCorners[0].y - 1))if (!losing.isPlaying) if (GameControl.audioEnabled) losing.Play();
        //Swapping x-Axis
        if (gameObject.transform.position.x < frustumCorners[0].x || (gameObject.transform.position.x > -1 * frustumCorners[0].x))
        {
            gameObject.transform.position = new Vector3(-1 * gameObject.transform.position.x + (1 / 50) * gameObject.transform.position.x, gameObject.transform.position.y, 0);
           
        }

        //Acceleration to xAxis
        float period = 0.0f;
        Vector3 accelerometerValue = Vector3.zero;
        Vector3 avg = Vector3.zero;
        DebugText = "Events:" + Input.acceleration.x + " " + SystemInfo.supportsGyroscope;
        for (int i = 0; i < Input.accelerationEvents.Length; i++)
        {
            accelerometerValue += Input.accelerationEvents[i].acceleration * Input.accelerationEvents[i].deltaTime;
            Debug.Log(accelerometerValue);
            period += Input.accelerationEvents[i].deltaTime;
            DebugText += " " + (period);

        }
        if (period != 0)
        {
            
            avg = accelerometerValue * (1 / period);
        }
        Debug.Log(avg);
        if (avg.magnitude > 1) avg = avg.normalized;
        direction = avg;
        direction = LowPassFilterAccelerometer();
        
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(direction.x * sideSpeed, gameObject.GetComponent<Rigidbody>().velocity.y, 0);
        Debug.Log(direction);
    }

    public Vector3 LowPassFilterAccelerometer()
    {

        lowPassValue = Vector3.Lerp(lowPassValue, direction, LowPassFilterFactor);
        return lowPassValue;
    }

    public string getDebug()
    {
        return DebugText;
    }

    //Coroutine Rocket Item
    IEnumerator Flight()
    {
        state = gameState.flying;
        if(GameControl.audioEnabled)  Camera.main.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2);
        Camera.main.GetComponent<AudioSource>().Stop();
        state = gameState.normal;
    }
    //Collisions
    private void OnTriggerEnter(Collider other)
    {
        //PlatformJumping
        // Zeile drunter sollte eigentlich gewährleiten, dass der Player nur von oben auf Platformen springen kann. Player sinkt jedoch trotzt continious Collision detection bei hoher geschwindigkeit ein. Deshalb nur test ob y-velocity< 0
        // -> Bug: Player kann seitlich an Platformen fallen und wird beschleunigt. Leider keine Lösung gefunden.
       //&& (gameObject.transform.position.y-gameObject.GetComponent<Collider>().bounds.extents.y)-(other.transform.position.y+other.gameObject.GetComponent<Collider>().bounds.extents.y)>-0.7f*other.gameObject.GetComponent<Collider>().bounds.extents.y)
        if (other.gameObject.tag == "Platform") if (gameObject.GetComponent<Rigidbody>().velocity.y < 0 )
            {
                gameObject.GetComponent<Rigidbody>().velocity = new Vector3(gameObject.GetComponent<Rigidbody>().velocity.x, jumpSpeed, 0);

                if (GameControl.audioEnabled) jumpSound.Play();
                if (other.gameObject.GetComponent<PlatformBehaviour>().modus == 2) Destroy(other.gameObject);
            }
        //EnemyCollision
        if (other.gameObject.tag == "Enemy")
        {
            if (state == gameState.normal)
            {
                if (gameObject.GetComponent<Rigidbody>().velocity.y < 0)
                {
                    gameObject.GetComponent<Rigidbody>().velocity = new Vector3(gameObject.GetComponent<Rigidbody>().velocity.x, jumpSpeed, 0);
                    other.gameObject.GetComponent<EnemyBehaviour>().playGrowl();
                    long[] pattern = new long[] { 0, 200, 100, 200, 100, 200 };
                    Vibration.Vibrate(pattern, -1);
                    Instantiate(expl, other.gameObject.transform);
                   if(GameControl.audioEnabled) growl.Play();
                    Destroy(other.gameObject);

                }
                else
                {
                    Destroy(gameObject);
                    SceneManager.LoadScene("Menu");
                }

            }
        }
        if (other.gameObject.tag == "RocketItem")
        {
            if (!GameControl.touch)
            {
                Destroy(other.gameObject);
                StartCoroutine("Flight");
            }
        }
    }

    public void PlayExplosion()
    {
        if (GameControl.audioEnabled) explosion.Play();
    }
 
}
