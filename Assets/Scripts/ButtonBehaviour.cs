﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//Menu Button
public class ButtonBehaviour : MonoBehaviour {
    private GameObject AudioButton;
    
    public Sprite on;
    public Sprite off;
    Image curIm;
    public static int audioControl=0;
    private void Start()
    {

        audioControl = 0;
        if (audioControl % 2 == 0) Camera.main.GetComponent<AudioSource>().Play();
    }

    public void LoadLevelByName(string name) {
        SceneManager.LoadScene(name);
    }
    public void changeAudio() {
        curIm = gameObject.GetComponent<Image>();

        if (audioControl % 2 == 0)
        {
            audioControl = 0;
            curIm.sprite = off;
            GameControl.audioEnabled = false;
            Camera.main.GetComponent<AudioSource>().Pause();
        }
        else {
            curIm.sprite = on;
            GameControl.audioEnabled = true;
            Camera.main.GetComponent<AudioSource>().Play();
        }
        audioControl++;
        Debug.Log(""+curIm.sprite.ToString());
    }
	
}
