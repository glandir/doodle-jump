﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBehaviour : MonoBehaviour {
    //Game Objects
    public GameObject Player;
    public GameObject Explosion;
    private AudioSource stretch;
    private AudioSource growlSource;
    
    public AudioClip growl;
    private GameObject movPlat;
    //Scaling Properties
    public float scaleSpeed = 0.5f;
    public float minScale = 0.1f;
    public float maxScale = 100.0f;
    private bool movingPlat=false;
    //Vibration
    private long[] EnemyDeadVibPattern = { 0, 300, 300, 300, 300, 300 };
    // Use this for initialization
    void Start () {
        //Audio
        growlSource = gameObject.AddComponent<AudioSource>();
        growlSource.clip = growl;
        growlSource.loop = false;
        growlSource.playOnAwake = false;
        growlSource.volume = 1.0f;
        stretch = gameObject.GetComponent<AudioSource>();
        

    }

    // Update is called once per frame
    void Update()
    {
        //Destroy Enemy out of view, should mayby be replaced by OnBecameInvisible 
        if (transform.position.y < Camera.main.transform.position.y + Player.GetComponent<PlayerBehaviour>().frustumCorners[0].y) Destroy(gameObject);

        if (movPlat == null) movingPlat = false;
       

        //ZoomInGesture
        if (Input.touchCount == 2)
        {
            if (!stretch.isPlaying) if (GameControl.audioEnabled) stretch.Play();
            //Get Two Touches
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);
            //Touch.position in WorldSpace
         //   GameControl.playerStats.text = "touchZero" + (touchZero.position-new Vector2(gameObject.transform.position.x,gameObject.transform.position.y));
            Vector3 touchZeroWorld = Camera.main.ScreenToWorldPoint(new Vector3(touchZero.position.x, touchZero.position.y, -1*Camera.main.transform.position.z));
            Vector3 touchOneWorld = Camera.main.ScreenToWorldPoint(new Vector3(touchOne.position.x, touchOne.position.y, -1 * Camera.main.transform.position.z));
            
            
            //ZoomIn on Enemy
            if ((Mathf.Abs(touchZeroWorld.x - gameObject.transform.position.x) < 0.5 && touchZeroWorld.y - gameObject.transform.position.y < 0.5f) || (Mathf.Abs(touchOneWorld.x - gameObject.transform.position.x) < 0.5f && touchOneWorld.y - gameObject.transform.position.y < 0.5f))
            {
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
                //Länge
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float deltaMagnitude = touchDeltaMag - prevTouchDeltaMag;
              //  GameControl.playerStats.text ="Mag:"+ deltaMagnitude;
                //Scaling
                this.transform.localScale += new Vector3(deltaMagnitude * scaleSpeed * Time.deltaTime,
                                            deltaMagnitude * scaleSpeed * Time.deltaTime,
                                            deltaMagnitude * scaleSpeed * Time.deltaTime);
                //Clamping
                this.transform.localScale = new Vector3(Mathf.Clamp(this.transform.localScale.x, minScale, maxScale),
                                                        Mathf.Clamp(this.transform.localScale.y, minScale, maxScale),
                                                        Mathf.Clamp(this.transform.localScale.z, minScale, maxScale));
                //MaxScale-> Destroy Enemy
                       if (gameObject.transform.localScale.x == maxScale)
                        {
                              float scale =gameObject.transform.localScale.x;
                              Vector3 pos = gameObject.transform.position;
                              Quaternion rot = Quaternion.identity;
                           Instantiate(Explosion, pos, rot);
                              Player.gameObject.GetComponent<PlayerBehaviour>().PlayExplosion();
                    Vibration.Vibrate(EnemyDeadVibPattern, -1);

                    GameControl.bonus += 10;
                              
                    Destroy(gameObject);
                }
            }
        }else if (stretch.isPlaying) stretch.Pause();
    }
  
    public void playGrowl() {
        growlSource.Play();
        

    }
    public void setPlayer(GameObject p)
    {
        Player = p;
    }
    

    private void OnBecameInvisible()
    {

        //Destroy Platforms out of view

        if (gameObject.transform.position.y < Camera.main.transform.position.y + Player.GetComponent<PlayerBehaviour>().frustumCorners[0].y)
        {


            Destroy(gameObject);
        }
    }

}
