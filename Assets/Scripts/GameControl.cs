﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameControl : MonoBehaviour
{


    //Game Objects
    public GameObject Platform;
    public GameObject Player;
    public GameObject Enemy;
    public GameObject RocketItem;
    public GameObject Sun;
    private GameObject curSun;
    
    //Platforms
    private float[] heights;
    private int arrayPointer=0;
    private float playerPrevPosY;
    private GameObject lastEnemy;
    private float platDistance = 1.5f;
    private int level=0;
    private float lastX;
    private float pDifficulty=0.0f;
    //GUI
    public static Text playerStats;
    public static int points=0;
    public static int bonus = 0;
    public static Text pCount;


    //Logic
   public static bool touch=false;
    public static bool audioEnabled=true;
    bool first = true;
    bool text = false;
    float endpos;
    public int pcounter=10;
    private int fingerId=-1;
    private bool ended=true;
    Touch prevTouch;
	// Use this for initialization
	void Start () {
        
        resetStats();
        playerStats = GameObject.Find("PlayerStats").GetComponent<Text>();
        playerStats.text = "Score: 0";
        pCount = GameObject.Find("pCount").GetComponent<Text>();
        
        heights = new float[50];
        playerPrevPosY = Player.transform.position.y;
       
	}

    // Update is called once per frame
    void Update() {
        //Load Menu when Player fails

        if (gameObject.transform.position.y - Player.transform.position.y > 50) {
            touch = false;
            first = true;
            SceneManager.LoadScene("Menu");
        }

        //LevelUp
        if (heights[arrayPointer] / 50 >= level)
        {
            level++;
            PlatformBehaviour.xVel += 0.2f;
            if(pDifficulty<0.5f)pDifficulty = level * 0.04f;
            if (platDistance < 4.5f) platDistance += 0.5f;
            
        }
        //Set touch true
        if (level % 5==0 && first == true && PlayerBehaviour.state == PlayerBehaviour.gameState.normal) touch = true;
        
        //TODO: NeuePlatform collsion mit Enemy
        //RahmenLogic

        //Normal routine
        if (!touch)
        {
            if (level % 5 != 0) { first = true; }
            
            //new Platforms&Enemies
            if (heights[arrayPointer] - Player.transform.position.y < 7)
            {
               


               //Platforms
                float nheight = Random.Range(heights[arrayPointer] + 1, heights[arrayPointer] + platDistance);
                arrayPointer = (arrayPointer + 1) % 50;
                heights[arrayPointer] = nheight;

                Vector3 platPos = new Vector3(Random.Range(-Player.GetComponent<PlayerBehaviour>().frustumCorners[0].x - 1, Player.GetComponent<PlayerBehaviour>().frustumCorners[0].x + 1), nheight, 0);
                //Prevent New Platform vs Enemy Collision
                if (lastEnemy != null)
                {
                    if ((Mathf.Abs(platPos.y - lastEnemy.transform.position.y) < 2.0f) && (Mathf.Abs(platPos.x - lastEnemy.transform.position.x) < 1.5f))
                    {
                        if (platPos.x <= 0) platPos.x = platPos.x + 3;
                        else platPos.x = platPos.x - 3;
                    }
                }

                GameObject plat = Instantiate(Platform, platPos, Quaternion.identity);
                lastX = plat.transform.position.x;
                if (level >= 2) if (Random.Range(0f, 1f) < pDifficulty) plat.GetComponent<PlatformBehaviour>().modus = 1;
                if (level >= 4) if (Random.Range(0f, 1f) < pDifficulty) plat.GetComponent<PlatformBehaviour>().modus = 2;
                
                //Enemy

                if (heights[arrayPointer] > 50)
                {
                    if (Random.Range(0, 1) < 0.5 && (arrayPointer % 20) == 0)
                    {
                        //RocketItem
                        if (arrayPointer % 100 == 0)
                        {
                            GameObject test = Instantiate(RocketItem, new Vector3(platPos.x, platPos.y + 0.75f, platPos.z), Quaternion.Euler(0, 0, 90));

                        }
                        //Enemy
                        else
                        {
                            lastEnemy = Instantiate(Enemy, new Vector3(platPos.x, platPos.y + 0.75f, platPos.z), Quaternion.Euler(0, 180, 0));
                            lastEnemy.GetComponent<EnemyBehaviour>().setPlayer(Player);
                         
                        }


                    }

                }





            }

        }

        //Placing Platforms by touching Routine
        else {

            if (first)
            {
                
                StartCoroutine("Touch");
            }
            //Debug string
            
            if (text) pCount.text = "Platforms: " + pcounter;
            //end of routine- > back to normal routine
            if (Player.transform.position.y > endpos+3f) {
                Destroy(curSun);
                touch = false;
                
                pCount.text = "";
                text = false;
                pcounter = 10;
                
                ended = true;
                fingerId = -1;
            }
            //Losing when Sun comes to close
            if (Player.transform.position.y < curSun.transform.position.y + 1)
            {
                playerStats.text = "melted!!";
                touch = false;
                first = true;
                SceneManager.LoadScene("Menu");
            }
            //Placing Platforms
            if (pcounter > 0 && Input.touchCount > 0)
            {
                 
                if (ended) {
                    ended = false;
                    
                    Touch t = Input.GetTouch(0);
                    pcounter--;
                    Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(t.position.x, t.position.y, Mathf.Abs(gameObject.transform.position.z)));
                    Instantiate(Platform, pos, Quaternion.identity);
                    prevTouch = t;
                    fingerId = t.fingerId;
               } 
            }

           
                for (int i = 0; i < Input.touchCount; i++)
                {
                    if (Input.GetTouch(i).fingerId == fingerId && Input.GetTouch(i).phase==TouchPhase.Ended) ended = true;
                }
            
            //first Platforms after routine
            if (heights[arrayPointer] - Player.transform.position.y < 7)
                {
                    



                    float nheight = Random.Range(heights[arrayPointer] + 1, heights[arrayPointer] + platDistance);
                    arrayPointer = (arrayPointer + 1) % 50;
                    heights[arrayPointer] = nheight;

                    if (heights[arrayPointer] > endpos)
                    {
                        Vector3 platPos = new Vector3(Random.Range(-Player.GetComponent<PlayerBehaviour>().frustumCorners[0].x - 1, Player.GetComponent<PlayerBehaviour>().frustumCorners[0].x + 1), nheight, 0);
                        GameObject plat = Instantiate(Platform, platPos, Quaternion.identity);
                        if (level >= 2) if (Random.Range(0f, 1f) < 0.1f) plat.GetComponent<PlatformBehaviour>().modus = 1;
                        if (level >= 4) if (Random.Range(0f, 1f) < 0.1f) plat.GetComponent<PlatformBehaviour>().modus = 2;
                    }
                }
            
        }
    }
    private void FixedUpdate()
    {
        //Score
        if (playerPrevPosY < Player.transform.position.y)
        {
            points = (int)Player.transform.position.y;
           playerStats.text = "Score: " + (points+bonus) ;
        }
        //CameraPosition
        if (Player.transform.position.y > playerPrevPosY)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, Player.transform.position.y, gameObject.transform.position.z);
            playerPrevPosY = gameObject.transform.position.y;
        }
        }
    private void resetStats()
    {
        points = 0;
        bonus = 0;
        pcounter = 10;
        touch = false;
        first = true;
        ended = true;
        fingerId = -1;
        PlatformBehaviour.xVel = 2.6f;
    }
    
 IEnumerator Touch(){
        //Istantiate 3xEnemies, control Text
        first = false;
         endpos = Player.transform.position.y + 40;
        Instantiate(Enemy, new Vector3(lastX, Player.transform.position.y + 18f, 0), Quaternion.Euler(0, 180, 0));
        Instantiate(Enemy, new Vector3(lastX+1, Player.transform.position.y + 18f, 0), Quaternion.Euler(0, 180, 0));
        Instantiate(Enemy, new Vector3(lastX-1, Player.transform.position.y + 18f, 0), Quaternion.Euler(0, 180, 0));
        int add1=0;
        if (lastX < 0) add1 = 2;
        else add1 = -2;
        Instantiate(Enemy, new Vector3(lastX+add1, Player.transform.position.y + 28f, 0), Quaternion.Euler(0, 180, 0));
        Instantiate(Enemy, new Vector3(lastX + add1+1, Player.transform.position.y + 28f, 0), Quaternion.Euler(0, 180, 0));
        Instantiate(Enemy, new Vector3(lastX + add1-1, Player.transform.position.y + 28f, 0), Quaternion.Euler(0, 180, 0));
        Instantiate(Enemy, new Vector3(lastX + add1/2+1, Player.transform.position.y + 38f, 0), Quaternion.Euler(0, 180, 0));
        Instantiate(Enemy, new Vector3(lastX + add1 / 2 -1, Player.transform.position.y + 38f, 0), Quaternion.Euler(0, 180, 0));
        Instantiate(Enemy, new Vector3(lastX + add1 / 2  , Player.transform.position.y + 38f, 0), Quaternion.Euler(0, 180, 0));
        pCount.text = "You have 10 Platforms.\nTouch to place them! \nDon't let the Sun burn you!!!";
        yield return new WaitForSeconds(4);
        curSun=Instantiate(Sun, new Vector3(0, Player.transform.position.y - 6.0f, 0), Quaternion.identity);
        if(level<=30)curSun.GetComponent<SunBehaviour>().sunVelocity+= (level/5-1)*0.25f;
        yield return new WaitForSeconds(2);
        text = true;
        
    }

 
}
