﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehaviour : MonoBehaviour {
    public GameObject Player;
	// Use this for initialization
	void Start () {
        Player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < Camera.main.transform.position.y + Camera.main.GetComponent<GameControl>().Player.GetComponent<PlayerBehaviour>().frustumCorners[0].y) Destroy(gameObject);
    }
}
